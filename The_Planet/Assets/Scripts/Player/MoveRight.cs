﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MoveRight : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public GameObject player;
    public Animator animator;


    private bool isBtnDown = false;

    private void Start()
    {
        animator = player.GetComponent<Animator>();
        animator.SetBool("isWalking", false);
    }


    private void Update()
    {
        if (isBtnDown)
        {
            player.transform.position += Vector3.right * 10 * Time.deltaTime;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isBtnDown = true;
        player.transform.localScale = new Vector3((float)1,(float)1, 3);
        animator.SetBool("isWalking", true);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isBtnDown = false;
        animator.SetBool("isWalking", false);
    }
}
