﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{
    void OnMouseDown()
    {
        Debug.Log("dd");
        StartCoroutine("startGame");
    }
    
    IEnumerator startGame()
    {
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene("InGameUI");
        SceneManager.LoadScene("InGameStage", LoadSceneMode.Additive);
        
    }

}
