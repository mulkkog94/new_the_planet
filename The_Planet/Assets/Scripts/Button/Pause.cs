﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    bool Ispause;

    void Start()
    {
        Ispause = false;    }

    public void PauseButton()
    {
        Debug.Log("유유");
        if (Ispause == false)
        {
            Time.timeScale = 0;
            Ispause = true;
            return;
        }
        if (Ispause == true)
        {
            Time.timeScale = 1;
            Ispause = false;
            return;
        }
    }
}

