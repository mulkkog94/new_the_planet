﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TitleButton : MonoBehaviour
{
    public GameObject SettingPanel;

    private void Start()
    {
        SettingPanel.SetActive(false);
    }

    public void StartGame()
    {
        SceneManager.LoadScene("StoryScene");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void OnSetting()
    {
        SettingPanel.SetActive(true);
    }

    public void OffSetting()
    {
        SettingPanel.SetActive(false);
    }


}
