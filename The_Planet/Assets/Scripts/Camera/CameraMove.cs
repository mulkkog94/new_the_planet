﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    public Transform targetTf;
    Vector3 refVeloV;

   
    void LateUpdate()
    {
        Vector3 tempV = Vector3.SmoothDamp(transform.position, targetTf.position, ref refVeloV, 0.5f);

        if (targetTf.position.x != 0)
        {
            tempV.z = -10f;
            tempV.y = 1f;
            transform.position = tempV;
        }

         tempV.z = -10f;
        tempV.y = 1f;
        transform.position = tempV;

    }

    
}
